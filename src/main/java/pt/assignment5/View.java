package pt.assignment5;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class View {
  JFrame startFrame = new JFrame("Start Frame");
  JFrame mainFrame = new JFrame("Config loaded succesfuly");
  List<MonitoredData> list = null;
  
  public View() {
    
    //List<MonitoredData> list = null;
    JLabel file = new JLabel("FileName: ");
    final JTextField fileField = new JTextField(30);
    fileField.setText("Activities.txt");
    JButton load = new JButton("Load file");
    startFrame.setLayout(new FlowLayout());
    startFrame.add(file);
    startFrame.add(fileField);
    startFrame.add(load);
    
    load.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        list = FileWriter.readFromFile(fileField.getText());
        boolean ok = list != null;
        for(int i = 0; ok && i < list.size(); i++) {
          if(list.get(i) == null) {
            ok = false;
          }
        }
        if(ok) {
          startFrame.setVisible(false);
          mainFrame.setVisible(true);
        } else {
          startFrame.setTitle("Invalid file");
          list = null;
        }
      }
    });
    
    mainFrame.setLayout(new GridLayout(1, 2));
    
    JTextArea textArea = new JTextArea();
    JScrollPane scrollPane = new JScrollPane(textArea);
    mainFrame.add(scrollPane);
    
    JPanel buttons = new JPanel();
    buttons.setLayout(new GridLayout(3, 2));
    
    JButton getTotalDays = new JButton("Get total days");
    JButton getActivityCount = new JButton("Get total activity count");
    JButton getDailyActivityCount = new JButton("Get daily activity count");
    JButton getLineDuration = new JButton("Get durations for each data entry");
    JButton getTotalActivityDuration = new JButton("Get total duration for each activity");
    JButton getShortActivities = new JButton("Get activities with <5 min duration");
    
    buttons.add(getTotalDays);
    buttons.add(getActivityCount);
    buttons.add(getDailyActivityCount);
    buttons.add(getLineDuration);
    buttons.add(getTotalActivityDuration);
    buttons.add(getShortActivities);
    
    mainFrame.add(buttons);
    
    getTotalDays.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        int nr = Worker.getNumberOfDays(list);
        textArea.setText("Total number of days monitored: " + nr + " Days");
        FileWriter.writeInFile(textArea.getText(), "1.txt");
      }
    });
    
    getActivityCount.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        Map<String,Long> map = Worker.getActivityCount(list);
        textArea.setText("Total activity couter:\n");
        for(String activity : map.keySet()){
          textArea.setText(textArea.getText() + activity + " - " + map.get(activity) + "\n");
        }
        FileWriter.writeInFile(textArea.getText(), "2.txt");
      }
    });
    
    getDailyActivityCount.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        Map<String,Long> map = Worker.getDailyFreq(list);
        List<String> days = new ArrayList<String>();
        days.addAll(map.keySet());
        Collections.sort(days);
        String currentDay = days.get(0).split(" ")[0];
        //System.out.println(currentDay);
        textArea.setText("Daily activity counter:\nDate: " + currentDay + ":\n");
        for(String d : days){
          String day = d.split(" ")[0];
          String activity = d.split(" ")[1];
          if(!day.equals(currentDay)) {
            currentDay = day;
            textArea.setText(textArea.getText() + "Date: " + currentDay + ":\n");
          }
          textArea.setText(textArea.getText() + "\t" + activity + " - " + map.get(d) + "\n");
        }
        FileWriter.writeInFile(textArea.getText(), "3.txt");
      }
    });
    
    getLineDuration.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        List<String> lines = Worker.getDurationsOfLines(list);
        textArea.setText("");
        for(String line : lines) {
          long duration = Long.parseLong(line.split(" : ")[1]);
          long seconds = duration % 60;
          long minutes = (duration / 60) % 60;
          long hours = (duration / 60) / 60;
          textArea.setText(textArea.getText() + line.split(" : ")[0] + " : " + hours + ":" + minutes + ":" + seconds + "\n");
        }
        FileWriter.writeInFile(textArea.getText(), "4.txt");
      }
    });
    
    getTotalActivityDuration.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        List<String> activities = Worker.getTotalDuration(list);
        textArea.setText("Total duration of each activity (HH:MM:SS):\n");
        for(String s : activities) {
          textArea.setText(textArea.getText() + s + "\n");
        }
        FileWriter.writeInFile(textArea.getText(), "5.txt");
      }
      
    });
    
    getShortActivities.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        List<String> activities = Worker.getShortActivities(list);
        textArea.setText("Activities that in 90% of cases have a duration less than 5 minutes:\n");
        for(String s : activities) {
          textArea.setText(textArea.getText() + s + "\n");
        }
        FileWriter.writeInFile(textArea.getText(), "6.txt");
      }
    });
    
    
    
    startFrame.setSize(550, 100);
    startFrame.setVisible(true);
    mainFrame.setSize(1000, 300);
    mainFrame.setVisible(false);   
    startFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }
  
  public static void main(String[] argv) {
    View view = new View();
  }

}
