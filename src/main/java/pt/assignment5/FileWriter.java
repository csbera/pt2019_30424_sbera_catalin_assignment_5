package pt.assignment5;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileWriter {
  public static List<MonitoredData> readFromFile(String filename) {
        List<MonitoredData> list = new ArrayList<MonitoredData>();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        try (Stream<String> stream = Files.lines(Paths.get(filename))) {

          list = stream
              .map(data -> data.split("\t\t"))
              .map(data -> {
                try {
                  return new MonitoredData(simpleDateFormat.parse(data[0]), simpleDateFormat.parse(data[1]), data[2].split("\t")[0]);
                }
                catch (ParseException e) {
                  return null;
                }
              })
              .collect(Collectors.toList());

        } catch (IOException e) {
          //e.printStackTrace();
          return null;
        } 
        //list.forEach(System.out::println);
        //System.out.println("Data Has been read"); 
    return list;
  }
  
  public static boolean writeInFile(String text, String filename) {
    Writer writer = null;

    try {
      writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream((filename))));
      writer.write(text);
    }
    catch (IOException ex) {
      return false;
    }
    finally {
      try {
        writer.close();
      }
      catch (IOException e) {
        return false;
      }
    }
    return true;
  }
}
