package pt.assignment5;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Worker {

  public static Map<String, Long> getActivityCount(List<MonitoredData> activities) {
    Map<String, Long> result = new HashMap<String, Long>();

    result = activities.stream().collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting()));
    
    return result;
  }

  public static int getNumberOfDays(List<MonitoredData> activities) {
    SimpleDateFormat dayDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Set<String> daySet = new HashSet<String>();
    // List<String> auxList = activities.stream().map(value ->
    // dayDateFormat.format(value.getStartTime())).collect(Collectors.toList());;
    daySet.addAll(
        activities.stream().map(value -> dayDateFormat.format(value.getStartTime())).collect(Collectors.toList()));
    daySet.addAll(
        activities.stream().map(value -> dayDateFormat.format(value.getEndTime())).collect(Collectors.toList()));
    return daySet.size();
  }

  public static List<String> getDurationsOfLines(List<MonitoredData> activities) {
    List<String> durations = new ArrayList<String>();
    // List<Long> durations = new ArrayList<Long>();
    durations = activities.stream().map(data -> data.getActivity() + " : " 
    + (data.getEndTime().getTime() - data.getStartTime().getTime())/1000).collect(Collectors.toList());
//    for (MonitoredData data : activities) {
//      long duration = data.getEndTime().getTime() - data.getStartTime().getTime();
//      duration = duration / 1000;
////      long seconds = duration % 60;
////      long minutes = (duration / 60) % 60;
////      long hours = (duration / 60) / 60;
//      // durations.add(duration);
//      durations.add(data.getActivity() + " : " + duration);
//      // System.out.println(data.getActivity() + " : " + hours + ":" + minutes + ":" + seconds);
//    }
    return durations;
  }

  public static List<String> getShortActivities(List<MonitoredData> activities) {
    List<String> result = new ArrayList<String>();
    List<String> lineDurations1 = Worker.getDurationsOfLines(activities);
    List<String> lineDurations2 = new ArrayList<String>(lineDurations1);

    Map<String, Long> totalActivities = new HashMap<>();
    Map<String, Long> shortActivities = new HashMap<>();
    
    totalActivities = lineDurations1.stream().map(data -> data.split(" : ")[0])
        .collect(Collectors.groupingBy(m -> m,Collectors.counting()));
    
    shortActivities = lineDurations2.stream().filter(data -> Long.parseLong(data.split(" : ")[1]) < 5*60).map(data -> data.split(" : ")[0])
        .collect(Collectors.groupingBy(m -> m,Collectors.counting()));
   
    

    for (Object activity : shortActivities.keySet()) {
      System.out.println(shortActivities.get(activity) + "-" + totalActivities.get(activity) + ":" + (float)(shortActivities.get(activity)) / (float)(totalActivities.get(activity)));
      if ((float)(shortActivities.get(activity)) / (float)(totalActivities.get(activity)) >= 0.9) {
        
        result.add((String)activity);
      }
    }

    // result.forEach(System.out::println);

    return result;
  }

  public static Map<String, Long> getDailyFreq(List<MonitoredData> activities) {
    SimpleDateFormat dayDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Map<String, Long> result = new HashMap<String, Long>();
    
    result = activities.stream().collect(Collectors.groupingBy(m -> dayDateFormat.format(m.getStartTime()) + " " + m.getActivity(),Collectors.counting()));
    
//    for (MonitoredData data : activities) {
//      String activity = data.getActivity();
//      String startDay = dayDateFormat.format(data.getStartTime()) + " " + activity;
//      String endDay = dayDateFormat.format(data.getEndTime()) + " " + activity;
//      if (!startDay.equals(endDay)) {
//        if (result.get(endDay) != null)
//          result.put(endDay, result.get(endDay) + 1);
//        else
//          result.put(endDay, 1);
//      }
//      if (result.get(startDay) != null)
//        result.put(startDay, result.get(startDay) + 1);
//      else
//        result.put(startDay, 1);
//    }

    return result;
  }

  public static List<String> getTotalDuration(List<MonitoredData> activities) {
    List<String> result = new ArrayList<String>();
    List<String> lineDurations = Worker.getDurationsOfLines(activities);

    Map<String, Long> totalActivities = new HashMap<String, Long>();
    
    totalActivities = lineDurations.stream()
        .collect(Collectors.groupingBy(m -> m.split(" : ")[0],Collectors.summingLong(m ->Long.parseLong(m.split(" : ")[1]) )));

//    for (String line : lineDurations) {
//      String activity = line.split(" : ")[0];
//      Long duration = Long.parseLong(line.split(" : ")[1]);
//
//      if (totalActivities.get(activity) != null)
//        totalActivities.put(activity, totalActivities.get(activity) + duration);
//      else
//        totalActivities.put(activity, duration);
//    }

    for (String activity : totalActivities.keySet()) {
      long duration = totalActivities.get(activity);
      long seconds = duration % 60;
      long minutes = (duration / 60) % 60;
      long hours = (duration / 60) / 60;
      result.add(activity + " : " + hours + ":" + minutes + ":" + seconds);
    }

    return result;
  }
}
